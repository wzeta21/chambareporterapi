using System.Collections.Generic;
using chambareporterApi.Entities;
using chambareporterApi.IServices;
using Microsoft.AspNetCore.Mvc;

namespace chambareporterApi.Controllers
{
    [Route("charep/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService userService;
        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpPost("auth")]
        [Produces("application/json", Type = typeof(User))]
        public IActionResult Post([FromBody]LoginUser user)
        {
            var usr = userService.Authenticate(user.Username, user.Password);

            if (usr == null)
                return BadRequest(new { message = "Usuario o contraseña incorrecto" });

            return Ok(usr);
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<User>))]
        public IActionResult Get()
        {
            return Ok(this.userService.GetAll());
        }

        [HttpPost]
        [Produces("application/json", Type = typeof(bool))]
        public IActionResult Post([FromBody]User user)
        {
            return Ok(userService.Create(user));
        }

        [HttpGet("{id}")]
        [Produces("application/json", Type = typeof(List<User>))]
        public IActionResult Get(int id)
        {
            return Ok(userService.GetById(id));
        }
    }
}
