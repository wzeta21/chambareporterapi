using System.Collections.Generic;
using chambareporterApi.Entities;
using chambareporterApi.IServices;
using Microsoft.AspNetCore.Mvc;

namespace chambareporterApi.Controllers
{
    [Route("charep/[controller]")]
    public class PositionController : Controller
    {
        private IPositionService positionService;
        public PositionController(IPositionService positionService)
        {
            this.positionService = positionService;
        }

        [HttpPost]
        [Produces("application/json", Type = typeof(bool))]
        public IActionResult Post([FromBody]Position position)
        {
            if (position == null)
                return BadRequest();
            return Ok(this.positionService.Create(position));
        }
        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<Position>))]
        public IActionResult Get()
        {
            return Ok(this.positionService.GetAll());
        }

        [HttpGet("{id}")]
        [Produces("application/json", Type = typeof(Position))]
        public IActionResult Get(int id)
        {
            return Ok(this.positionService.GetById(id));
        }
    }
}