using chambareporterApi.IServices;
using chambareporterApi.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;

namespace chambareporterApi.Controllers
{
    [Route("charep/[controller]")]
    public class ReportController : Controller
    {
        private IReportService reportService;
        private readonly IHostingEnvironment hostingEvironmento;
        public ReportController(
            IReportService reportService,
            IHostingEnvironment hostingEnvironment)
        {
            this.reportService = reportService;
            this.hostingEvironmento = hostingEnvironment;
        }

        [HttpPost]
        [RequestSizeLimit(90_000_000)]
        [Produces("application/json", Type = typeof(bool))]
        public IActionResult Post()
        {
            bool res = false;
            var repo = HttpContext.Request.Form["report"];
            Report report = JsonConvert.DeserializeObject<Report>(repo);

            var picturePostedFile = HttpContext.Request.Form.Files.GetFile("picture");
            string pictureUrl = this.SavePicture(picturePostedFile);

            if (pictureUrl != null)
            {
                report.PictureUrl = pictureUrl;
                res = reportService.Create(report);
            }

            return Ok(res);
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<Report>))]
        public IActionResult Get()
        {
            return Ok(reportService.GetAll());
        }

        [HttpGet("{id}")]
        [Produces("application/json", Type = typeof(Report))]
        public IActionResult Get(int id)
        {
            return Ok(reportService.GetById(id));
        }

        private string SavePicture(IFormFile picture)
        {

            bool res = false;
            string pictureName = new String(
                Path.GetFileNameWithoutExtension(picture.FileName)
                .Take(Path.GetFileNameWithoutExtension(picture.FileName).Length)
                .ToArray()
            );
            pictureName = pictureName + Path.GetExtension(picture.FileName);

            var filePath = hostingEvironmento.WebRootPath + "/media/pictures/" + pictureName;
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                picture.CopyTo(stream);
                res = true;
            }
            return res ? filePath : string.Empty;
        }
    }
}