using System.Collections.Generic;
using chambareporterApi.Entities;
using chambareporterApi.IServices;
using Microsoft.AspNetCore.Mvc;

namespace chambareporterApi.Controllers{
    [Route("charep/[controller]")]
    public class EmployeeController: Controller{

        private IEmployeeService empService;

        public EmployeeController(IEmployeeService employeeService){
            this.empService = employeeService;
        }

        [HttpPost]
        [Produces("application/json", Type = typeof(bool))]
        public IActionResult Post([FromBody]Employee employee){
            return Ok(empService.Create(employee));
        }
        
        [HttpGet]
        [Produces("application/json", Type = typeof(List<Employee>))]
        public IActionResult Get(){
            return Ok(empService.GetAll());
        }

        [HttpGet("{id}")]
        [Produces("application/json", Type = typeof(Employee))]
        public IActionResult Get(int id){
            return Ok(empService.GetById(id));
        }
    }
}