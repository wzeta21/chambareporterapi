﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using chambareporterApi.Data;

namespace deployment_app.Migrations
{
    [DbContext(typeof(ChambaRepContext))]
    partial class ChambaRepContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity("chambareporterApi.Entities.Employee", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("IdPosition");

                    b.Property<string>("LastName");

                    b.Property<string>("Name");

                    b.HasKey("Id")
                        .HasName("pk_employee");

                    b.HasIndex("Id")
                        .IsUnique()
                        .HasName("idx_employee");

                    b.HasIndex("IdPosition");

                    b.ToTable("Employees");
                });

            modelBuilder.Entity("chambareporterApi.Entities.Position", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id")
                        .HasName("pk_positions");

                    b.HasIndex("Id")
                        .IsUnique()
                        .HasName("idx_position");

                    b.ToTable("Positions");
                });

            modelBuilder.Entity("chambareporterApi.Entities.Report", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BeginTime");

                    b.Property<string>("ClientMail");

                    b.Property<string>("ClientName");

                    b.Property<string>("Date");

                    b.Property<string>("Description");

                    b.Property<int>("EmployeeId");

                    b.Property<string>("EndTime");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("PictureUrl");

                    b.Property<string>("Pictures");

                    b.Property<string>("WorkName");

                    b.HasKey("Id")
                        .HasName("pk_report");

                    b.HasIndex("EmployeeId");

                    b.HasIndex("Id")
                        .IsUnique()
                        .HasName("idx_report");

                    b.ToTable("Reports");
                });

            modelBuilder.Entity("chambareporterApi.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("Email");

                    b.Property<int>("IdEmployee");

                    b.Property<string>("Login");

                    b.Property<string>("Name");

                    b.Property<string>("Password");

                    b.Property<bool>("Status");

                    b.Property<string>("Surname");

                    b.HasKey("Id")
                        .HasName("pk_usr");

                    b.HasIndex("Id")
                        .IsUnique()
                        .HasName("idx_usr");

                    b.HasIndex("IdEmployee")
                        .IsUnique();

                    b.ToTable("Users");
                });

            modelBuilder.Entity("chambareporterApi.Entities.Employee", b =>
                {
                    b.HasOne("chambareporterApi.Entities.Position", "Position")
                        .WithMany("Employees")
                        .HasForeignKey("IdPosition")
                        .HasConstraintName("fk_employee_positions")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("chambareporterApi.Entities.Report", b =>
                {
                    b.HasOne("chambareporterApi.Entities.Employee", "Employee")
                        .WithMany("Reports")
                        .HasForeignKey("EmployeeId")
                        .HasConstraintName("fk_employee_reports")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("chambareporterApi.Entities.User", b =>
                {
                    b.HasOne("chambareporterApi.Entities.Employee", "Employee")
                        .WithOne("User")
                        .HasForeignKey("chambareporterApi.Entities.User", "IdEmployee")
                        .HasConstraintName("fk_user_employee")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
