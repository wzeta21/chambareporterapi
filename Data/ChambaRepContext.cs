using chambareporterApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace chambareporterApi.Data
{
    public class ChambaRepContext : DbContext
    {
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Employee> Employees {get; set;}
        public virtual DbSet<Position> Positions {get;set;}
        public virtual DbSet<Report> Reports {get; set;}

        public ChambaRepContext(DbContextOptions<ChambaRepContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>(entity =>{
                entity.HasIndex(e => e.Id)
                    .HasName("idx_employee")
                    .IsUnique();
                entity.HasKey(e => e.Id)
                    .HasName("pk_employee");
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
                entity.HasOne(o => o.Position)
                    .WithMany(m =>m.Employees)
                    .HasForeignKey(f => f.IdPosition)
                    .HasConstraintName("fk_employee_positions");           
            }); 

            modelBuilder.Entity<User>(entity =>{
                entity.HasIndex(i => i.Id)
                    .HasName("idx_usr")
                    .IsUnique();
                entity.HasKey(k => k.Id)
                    .HasName("pk_usr");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.HasOne(o => o.Employee)
                    .WithOne(m => m.User)
                    .HasForeignKey<User>(f => f.IdEmployee)
                    .HasConstraintName("fk_user_employee");
            });   

            modelBuilder.Entity<Position>(entity => {
                entity.HasIndex(i => i.Id)
                    .HasName("idx_position")
                    .IsUnique();

                entity.HasKey(k => k.Id)
                    .HasName("pk_positions");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });   

            modelBuilder.Entity<Report>(entity => {
                entity.HasIndex(i => i.Id)
                    .HasName("idx_report")
                    .IsUnique();
                entity.HasKey(k => k.Id)
                    .HasName("pk_report");
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
                entity.HasOne(o => o.Employee)
                    .WithMany(m => m.Reports)
                    .HasForeignKey(f => f.EmployeeId)
                    .HasConstraintName("fk_employee_reports");
            });     
        }
    }
}
