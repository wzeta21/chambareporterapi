FROM mcr.microsoft.com/dotnet/core/runtime:2.2

ENV TZ=America/La_Paz
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /app

ADD ./artifact/ ./
EXPOSE 80

# ENTRYPOINT ["./deployment-app"]

CMD ["./deployment-app"]
