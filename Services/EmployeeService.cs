using System;
using System.Collections.Generic;
using System.Linq;
using chambareporterApi.Data;
using chambareporterApi.Entities;
using chambareporterApi.IServices;
using Microsoft.EntityFrameworkCore;

namespace chambareporterApi.Services
{
    public class EmployeeService : IEmployeeService
    {
        private ChambaRepContext db;
        public EmployeeService(ChambaRepContext db)
        {
            this.db = db;
        }

        public bool Create(Employee employee)
        {
            bool resp = false;
            try
            {
                employee.Position = null;
                employee.User = null;
                db.Add(employee);
                db.SaveChanges();
                resp = true;
            }
            catch (DbUpdateException)
            {
                throw new System.Exception("Error creating a user");
            }
            return resp;
        }

        public IEnumerable<Employee> GetAll()
        {
            return db.Employees.AsEnumerable() ?? null;
        }

        public Employee GetById(int id)
        {
            return db.Employees.Where(x => x.Id == id).FirstOrDefault();
        }

        public bool Update(Employee employee)
        {
            throw new System.NotImplementedException();
        }
        private int GetMaxId()
        {
            int id = 0;
            try
            {
                IEnumerable<int> ids = db.Employees.Select(x => x.Id).ToList<int>();
                if (ids.Count() > 0)
                {
                    id = ids.Max();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ", Inner exception: " + ex.InnerException.ToString());
            }
            return id;
        }
    }
}