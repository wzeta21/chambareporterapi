using System;
using System.Collections.Generic;
using System.Linq;
using chambareporterApi.Data;
using chambareporterApi.Entities;
using chambareporterApi.IServices;

namespace chambareporterApi.Services {
    public class ReportService : IReportService
    {
        private ChambaRepContext db;
        public ReportService(ChambaRepContext db){
            this.db = db;
        }
        public bool Create(Report report)
        {
            bool resuslt = false;
            try
            {
                report.Employee = null;
                db.Add(report);
                db.SaveChanges();
                resuslt = true;
            }
            catch (Exception ex)
            {
                
                throw new Exception(ex.Message);
            }
            return resuslt;
        }

        public IEnumerable<Report> GetAll()
        {
            return db.Reports.AsEnumerable() ?? null;
        }

        public Report GetById(int id)
        {
            return db.Reports.Where(x => x.Id == id).FirstOrDefault();
        }

        public bool Update(Report report)
        {
            throw new System.NotImplementedException();
        }
    }
}