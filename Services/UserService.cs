using System;
using System.Collections.Generic;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using chambareporterApi.IServices;
using chambareporterApi.Data;
using chambareporterApi.Entities;
using Microsoft.EntityFrameworkCore;
using chambareporterApi.Helpers;

namespace chambareporterApi.Services
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private ChambaRepContext db;

        public UserService(ChambaRepContext db, IOptions<AppSettings> appSettings)
        {
            this.db = db;
            _appSettings = appSettings.Value;
        }
        public Session Authenticate(string username, string password)
        {
            Session session;
            try
            {
                var all = db.Users.AsEnumerable();
                var user = all.Where(x => x.Login == username && x.Password == SHATrasform.ToSHA512(password))
                              .FirstOrDefault();

                if (user == null)
                    return null;

                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[] {
                  new Claim (ClaimTypes.Name, user.Id.ToString ())
                }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);

                user.Password = null;
                session = new Session() { User = user, Token = tokenHandler.WriteToken(token) };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
            return session ?? null;
        }
        public IEnumerable<User> GetAll()
        {
            return db.Users.AsEnumerable<User>().Select(x =>
            {
                x.Password = null;
                return x;
            });
        }
        public bool Create(User user)
        {
            bool resp = false;
            try
            {
                user.Password = SHATrasform.ToSHA512(user.Password);
                user.Status = true;
                user.CreatedAt = DateTime.Now;
                user.Employee = null;
                db.Add(user);
                db.SaveChanges();
                resp = true;
            }
            catch (DbUpdateException ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
            return resp;
        }

        public User GetById(int id)
        {
            return db.Users.Where(x => x.Id == id).FirstOrDefault() ?? null;
        }

        public User Get(User user)
        {
            return db.Users.Where(x => x.Id == user.Id)
                .Include(y => y.Employee)
                    .ThenInclude(p => p.Position)
                .FirstOrDefault() ?? null;
        }

        public bool Update(User user)
        {
            throw new System.NotImplementedException();
        }

        private int GetMaxId()
        {
            int id = 0;
            try
            {
                List<int> ids = db.Users.Select(x => x.Id).ToList<int>();
                if (ids.Count() > 0)
                {
                    id = ids.Max();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ", Inner exception, getting maxId from user: " + ex.InnerException.ToString());
            }
            return id;
        }
    }
}
