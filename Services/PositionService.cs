using System;
using System.Collections.Generic;
using System.Linq;
using chambareporterApi.Data;
using chambareporterApi.Entities;
using chambareporterApi.IServices;
using Microsoft.EntityFrameworkCore;

namespace chambareporterApi.Services
{
    public class PositionService : IPositionService
    {
        private ChambaRepContext db;

        public PositionService(ChambaRepContext db)
        {
            this.db = db;
        }
        public bool Create(Position position)
        {
            bool resp = false;
            try
            {
                position.Employees = null;
                db.Add(position);
                db.SaveChanges();
                resp = true;
            }
            catch (DbUpdateException)
            {
                throw new System.Exception("Error creating a position");
            }
            return resp;
        }

        public IEnumerable<Position> GetAll()
        {
            return this.db.Positions.AsEnumerable() ?? null;
        }

        public Position GetById(int id)
        {
            return this.db.Positions.Where(x => x.Id == id).FirstOrDefault() ?? null;
        }
        private int GetMaxId()
        {
            int id = 0;
            try
            {
                IEnumerable<int> ids = db.Positions.Select(x => x.Id).ToList<int>();
                if (ids.Count() > 0)
                {
                    id = ids.Max();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ", Inner exception: " + ex.InnerException.ToString());
            }
            return id;
        }
    }
}