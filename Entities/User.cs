using System;

namespace chambareporterApi.Entities
{
    public class User: Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int IdEmployee { get; set; }
        public Employee Employee { get; set; }
    }
}
