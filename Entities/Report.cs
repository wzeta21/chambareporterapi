using System;

namespace chambareporterApi.Entities
{
    public class Report : Entity
    {
        public int EmployeeId { get; set; }
        public string WorkName { get; set; }
        public string Date {get; set;}
        public string BeginTime { get; set; }
        public string EndTime { get; set; }
        public string ClientName { get; set; }
        public string ClientMail { get; set; }
        public string Description { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Pictures { get; set; }
        public string PictureUrl {get; set;}
        public Employee Employee { get; set; }

    }
}