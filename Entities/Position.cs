using System.Collections.Generic;

namespace chambareporterApi.Entities
{
    public class Position : Entity
    {
        public Position()
        {
            this.Employees = new HashSet<Employee>();
        }
        public string Name { get; set; }
        public ICollection<Employee> Employees { get; set; }
    }
}