namespace chambareporterApi.Entities
{
    public class Session
    {
        public string Token { get; set; }
        public User User { get; set; }
    }
}
