

using System.Collections.Generic;

namespace chambareporterApi.Entities
{
    public class Employee : Entity
    {

        public Employee()
        {
            this.User = new User();
            this.Reports = new HashSet<Report>();
        }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int IdPosition { get; set; }
        public Position Position { get; set; }

        public User User { get; set; }

        public ICollection<Report> Reports { get; set; }
    }
}