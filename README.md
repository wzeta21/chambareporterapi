# Deployment App

## Public app

>dotnet publish -c Release -r ubuntu.18.04-x64

## Step to run this app

1. dotnet publish -c Release -r ubuntu.18.04-x64
3. docker build -t chambareportes:2.2 .
4. docker run -it -d --name socket-ui -p 8080:80 -v $(pwd)/bin/Release/netcoreapp2.2/ubuntu.18.04-x64/publish/:/app/ chambareportes:2.2
5. In a web browser put http://localhost:8080/

## Empezar a trabajar con asp.net core 2
    1. Instalar SDK .net core 2.0 o superio
    2. Abrir el proyecto con vscode u otro IDE
    3. ejecutar: dotnet restore <- esto instala las librerias necesarias

## Generar la base de datos

Para generar la base de datos siga los siguientes pasos

    1.  dotnet ef migrations add chambareporterdb
    2.  dotnet ef database update

## Ejecucíon/construcción
* para construir el proyecto: dotnet build
* para ejecutar el proyecto: dotnet run


## compilar/publicar proyecto:
Para publicar un release siga los siguientes pasos, segun su sitema operativo
- dotnet publish -c Release -r ubuntu.16.04-x64 --self-contained <- for ubuntu with dependecies
- dotnet publish -c Release -r ubuntu.16.04-x64 <- para ubuntu without dependecies
- dotnet publish -c Release -r win7-x86