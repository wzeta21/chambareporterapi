using System;
using System.Security.Cryptography;
using System.Text;

namespace chambareporterApi.Helpers
{
    public class SHATrasform
    {
        public static string ToSHA1(string origin)
        {
            string sha1 = string.Empty;
            using (var sha256 = SHA1.Create())
            {
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(origin));
                sha1 = BitConverter.ToString(hashedBytes).Replace("-", "");
            }
            return sha1;
        }
        public static string ToSHA512(string origin)
        {
            string sha512 = string.Empty;
            using (var sha = SHA512.Create())
            {
                var hashedBytes = sha.ComputeHash(Encoding.UTF8.GetBytes(origin));
                sha512 = BitConverter.ToString(hashedBytes).Replace("-", "");
            }
            return sha512;
        }
    }
}
