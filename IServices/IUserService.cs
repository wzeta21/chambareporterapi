using System.Collections.Generic;
using chambareporterApi.Entities;

namespace chambareporterApi.IServices
{
    public interface IUserService
    {
        Session Authenticate(string username, string password);
        IEnumerable<User> GetAll();
        User GetById(int id);
        User Get(User user);
        bool Create(User user);
        bool Update(User user);
    }
}
