using System.Collections.Generic;
using chambareporterApi.Entities;
namespace chambareporterApi.IServices {
    public interface IPositionService
    {       
        IEnumerable<Position> GetAll();
        Position GetById(int id);
        bool Create(Position position);
    }
}