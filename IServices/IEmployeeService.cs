using System.Collections.Generic;
using chambareporterApi.Entities;

namespace chambareporterApi.IServices{
    public interface IEmployeeService
    {
        IEnumerable<Employee> GetAll();
        Employee GetById(int id);
        bool Create(Employee employee);
        bool Update(Employee employee);
    }
}