using System.Collections.Generic;
using chambareporterApi.Entities;

namespace chambareporterApi.IServices {
    public interface IReportService
    {
        IEnumerable<Report> GetAll();
        Report GetById(int id);
        bool Create(Report report);
        bool Update(Report report);
    }
}