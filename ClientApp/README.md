# SocketUi

This project is to implement a UI for python socket.

## Step to run this app

1. npm install
2. ng build --prod
3. docker build -t socket-nginx:1.0.0 .
4. docker run -it -d --name socket-ui -p 8080:80 socket-nginx:1.0.0
5. In a web browser put http://localhost:8080/
