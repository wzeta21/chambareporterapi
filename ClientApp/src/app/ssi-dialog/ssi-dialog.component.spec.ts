import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SsiDialogComponent } from './ssi-dialog.component';

describe('SsiDialogComponent', () => {
  let component: SsiDialogComponent;
  let fixture: ComponentFixture<SsiDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SsiDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SsiDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
