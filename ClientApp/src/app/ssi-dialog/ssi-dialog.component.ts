import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  group: string;
  node: string;
  flag: number;
  action: string;
  target: number;
}

@Component({
  selector: 'app-ssi-dialog',
  templateUrl: './ssi-dialog.component.html',
  styleUrls: ['./ssi-dialog.component.css']
})
export class SsiDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SsiDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
  }
  deploy() {
    console.log('start deploy');
  }
  cancel() {
    this.dialogRef.close();
  }

}
