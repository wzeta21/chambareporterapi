import { User } from './../core/models/user.model';
import { StorageService } from './../core/services/storage.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SsiDialogComponent } from '../ssi-dialog/ssi-dialog.component';
import { SocketService } from '../core/services/socket.service';
import { Project } from '../core/models/project.model';
import { AuthenticationService } from '../core/services/authentication.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: User;
  projects: Project[];

  constructor(
    public dialog: MatDialog,
    private authenticationService: AuthenticationService,
    private storageService: StorageService,
    private socketService: SocketService
  ) { }

  ngOnInit() {
    this.init();
    this.user = this.storageService.getCurrentUser();
    this.projects = this.socketService.getProjects();
    console.log(JSON.stringify(this.projects));    
  }
  init() {
    this.user = new User();
    this.projects = [];
  }

  executeDeploy(group: string) {
    const dialogRef = this.dialog.open(SsiDialogComponent, {
      width: '350px',
      data: this.projects.find( item => item.group === group)
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const project = this.projects.find( item => item.group === group);
        console.log(JSON.stringify(project));
        this.socketService.startDeploy(project);
      }
    });
  }

  logout() {
    console.log('Saliendo');
    this.authenticationService.logout().subscribe(
      response => {
        if (response) {
          this.storageService.logout();
        }
      }
    );
  }
}
