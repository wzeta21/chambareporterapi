import { Project } from '../models/project.model';

export const PROJECTS: Project[] = [
  { group: 'Portability', node: '10.40.21.91', flag: 8, action: 'portables', target: 94 },
  { group: 'Massive', node: '10.40.21.92', flag: 8, action: 'masivos', target: 100 },
  { group: 'External', node: '10.40.21.93', flag: 8, action: 'externos', target: 60 },
  { group: 'Critics', node: '10.40.21.94', flag: 8, action: 'criticos', target: 94 },
  { group: 'Async', node: '10.40.21.95', flag: 8, action: 'asincronos', target: 10 },
  { group: 'Async projects', node: '10.40.21.96', flag: 8, action: 'proyectos_asincronos', target: 80 }
];
