import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let currentSession = JSON.parse(localStorage.getItem('currentUser'));
    if (currentSession && currentSession.token) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${currentSession.token}`
        }
      });
    }

    return next.handle(req);
  }
}
