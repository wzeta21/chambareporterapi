// import { fakeBackendProvider } from './helper/fake-backend';
import { AuthorizatedGuard } from './guards/authorizated.guard';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StorageService } from './services/storage.service';
import { SocketService } from './services/socket.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    StorageService,
    AuthorizatedGuard,
    // fakeBackendProvider,
    SocketService
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
