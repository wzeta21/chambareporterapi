export class Project {
  group: string;
  node: string;
  flag: number;
  action: string;
  target: number;
}
