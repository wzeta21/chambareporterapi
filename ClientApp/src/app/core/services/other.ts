// import { Injectable } from '@angular/core';
// import { HttpClient, HttpHeaders } from '@angular/common/http';

// @Injectable({
//     providedIn: 'root'
// })
// export class RestService {

//     constructor(private http: HttpClient) { }

//     simpleGET(url: string): Promise<any> {
//         return new Promise((resolve, reject) => {
//             this.http.get(url, {
//                 headers: new HttpHeaders().set('Authorization', this.extractToken()),
//             }).subscribe(
//                 response => {
//                     resolve(response);
//                 },
//                 error => {
//                     reject(error);
//                 });
//         });
//     }

//     simplePOST(url: string): Promise<any> {
//         return new Promise((resolve, reject) => {
//             this.http.post(url, null, {
//                 headers: new HttpHeaders().set('Authorization', this.extractToken()),
//             }).subscribe(
//                 response => {
//                     resolve(response);
//                 },
//                 error => {
//                     reject(error);
//                 });
//         });
//     }

//     getForFile(url: string): Promise<any> {
//         return new Promise((resolve, reject) => {
//             this.http.get(url, {
//                 headers: new HttpHeaders({
//                     Authorization: this.extractToken(),
//                     'Content-Type': 'application/octet-stream',
//                     Accept: 'application/octet-stream'
//                 })
//             }).subscribe(
//                 response => {
//                     resolve(response);
//                 },
//                 error => {
//                     reject(error);
//                 });
//         });
//     }
// }