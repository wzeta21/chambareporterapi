import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Session } from '../../core/models/session.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginUser } from 'src/app/core/models/login-object.model';
import { environment } from 'src/environments/environment';

const BASE_URL = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private http: HttpClient
  ) { }

  login(loginObj: LoginUser): Observable<Session> {
    return this.http.post<Session>(`${BASE_URL}users/auth`, loginObj);
  }

  // login(loginObj: LoginUser): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     this.http.post(`${BASE_URL}users/auth`, loginObj, {})
  //       .subscribe(
  //         response => {
  //           resolve(response);
  //         },
  //         error => {
  //           reject(error);
  //         });
  //   });
  // }

  logout(): Observable<boolean> {

    return new Observable(observer => {
      localStorage.removeItem('currentUser');
      observer.next(true);
    });
  }
}
