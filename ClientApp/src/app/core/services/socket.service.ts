import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PROJECTS } from '../mocks/mock-projects';
import { Project } from '../models/project.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const BASE_URL = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor(private http: HttpClient) { }

  getProjects(): Project[] {
    return PROJECTS;
  }

  startDeploy(project: Project): boolean {
    const param = `v_flag=${project.flag}&v_action=${project.action}&target=${project.target}`;
    console.log(param);
    return true;
  }
}
