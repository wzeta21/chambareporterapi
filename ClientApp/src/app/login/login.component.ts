import { Session } from './../core/models/session.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginUser } from '../core/models/login-object.model';
import { StorageService } from '../core/services/storage.service';
import { AuthenticationService } from '../core/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public submitted = false;
  public error: { code: number, message: string } = null;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private storageService: StorageService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.storageService.removeCurrentSession();
  }

  public submitLogin() {
    this.submitted = true;
    this.error = null;

    if (this.loginForm.invalid) {
      return;
    }

    this.authenticationService.login(new LoginUser(this.loginForm.value))
      .subscribe((data: Session) => {
        if (data) {
          this.storageService.setCurrentSession(data);
          this.router.navigate(['/home']);
        }
      },
        error => this.error = JSON.parse(error._body)
      );
  }
}
